public class Inmueble {

	private String direccion;
	private String nombre;
	private String apellido;
	private String localidad;
	private String telefono;
	private Double cantm2;

	private int id;

	public Inmueble(String nombre, String apellido, String telefono, String localidad, String direccion,
			Double cantm2) {
		this.direccion = direccion;
		this.nombre = nombre;
		this.apellido = apellido;
		this.localidad = localidad;
		this.telefono = telefono;
		this.cantm2 = cantm2;

	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getCantm2() {
		return cantm2;
	}

	public void setCantm2(Double cantm2) {
		this.cantm2 = cantm2;
	}

}