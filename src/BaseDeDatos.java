import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.management.Query;

public class BaseDeDatos {

	public static void agregarInmueble(String nombre, String apellido, String telefono, String localidad,
			String direccion, Double m2) throws SQLException, ClassNotFoundException {
		try {

			// Conexión a la base de datos dando como parámetros el String de conexion y
			// el
			// Driver.

			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abminmueble";
			Class.forName(myDriver);

			// Parámetros para crear la conexión con la base de datos, usuario y
			// contraseña.
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "insert into inmueble (nombre, apellido, telefono, localidad, direccion, m2)"
					+ " values (? , ? , ?, ? , ? , ?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, nombre);
			preparedStmt.setString(2, apellido);
			preparedStmt.setString(3, telefono);
			preparedStmt.setString(4, localidad);
			preparedStmt.setString(5, direccion);
			preparedStmt.setDouble(6, m2);

			// Ejecuto el preparedStatement, de esta manera, inserto los valores a la base
			// de datos
			preparedStmt.executeUpdate();
			// cierro la conexión.
			conn.close();
		} catch (SQLException e) {
			System.out.println("Se ha generado la siguiente excepción");
			System.out.println(e.getMessage());
			throw e;
		}
	}

	public void updateInmueblePorId(int id, String nombre, String apellido, String telefono, String localidad,
			String direccion, Double m2) throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abminmueble";
			Class.forName(myDriver);

			Connection conn2 = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "UPDATE inmueble SET nombre = ?, apellido = ?, telefono = ?, localidad = ?, direccion = ?, m2 = ? WHERE id = ?";
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query);
			preparedStmt2.setString(1, nombre);
			preparedStmt2.setString(2, apellido);
			preparedStmt2.setString(3, telefono);
			preparedStmt2.setString(4, localidad);
			preparedStmt2.setString(5, direccion);
			preparedStmt2.setDouble(6, m2);
			preparedStmt2.setInt(7, id);

			preparedStmt2.executeUpdate();
			preparedStmt2.close();

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Se ha generado la siguiente excepción:");
			System.out.println(e.getMessage());
		}

	}
}
